FROM debian:buster-slim

RUN export DEBIAN_FRONTEND=noninteractive && \
apt update -y && \
apt -o Dpkg::Options::="--force-confold" install samba avahi-daemon -y 

COPY ./conf/samba/smb.conf /etc/samba/smb.conf
COPY ./conf/avahi/timemachine.service /etc/avahi/services/
COPY ./conf/avahi/avahi-daemon.conf /etc/avahi

COPY healthcheck.sh healthcheck.sh


COPY init.sh init.sh


