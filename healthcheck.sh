#!/bin/bash

#checks services in int.d

function svc_chk (){
svc=$1
if  /etc/init.d/$svc status 2>&1 |  grep "fail" ; then 
	>&2 echo "[FAIL] $svc not running"
	exit 1
fi

}

svc_chk smbd
svc_chk dbus
svc_chk avahi-daemon 
