#!/bin/bash


#sync uids
groupadd -g $G_UID timemachine
useradd -u $U_UID -g $G_UID timemachine


#start messagebus
#must be before avahi
/etc/init.d/dbus start

#start samba
/etc/init.d/smbd start


#not required as we use avahi
#/etc/init.d/nmbd start

#start avahi
/etc/init.d/avahi-daemon start


while true
do
	#allow broadcasting across VLANs, seems to die out otherwise
	echo "Restarting Avahi to fix broadbasting bug."
	/etc/init.d/avahi-daemon restart
	sleep 30
done

#easy hack to leave running
tail -f /dev/null
